<?php

use Illuminate\Database\Seeder;
use App\Product;
use Faker\Factory as Faker;


class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        Product::create([
            'name' => $faker->name,
            'price' => $faker->randomFloat(2),
            'description' => $faker->text
        ]);

    }
}