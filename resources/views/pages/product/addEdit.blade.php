@extends('layouts.default')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>
                    @if(isset($product))
                        Product Edit
                    @else
                        Product create
                    @endif
                </h4>
                <form action="@if(isset($product)){{route('products.update',['id' => encrypt($product['id'])])}}@else{{route('products.store')}}@endif" method="POST" accept-charset="UTF-8" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="@if(isset($product)){{$product['name']}}@else{{old('nome')}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="name">Descricao:</label>
                        <textarea id="description" name="description" class="form-control" placeholder="Enter description">@if(isset($product)){{$product['description']}}@else{{old('description')}}@endif</textarea>
                    </div>

                    <div class="form-group">
                        <label for="name">Price:</label>
                        <input type="number" class="form-control" id="price" placeholder="Enter price" name="price" value="@if(isset($product)){{$product['price']}}@else{{old('price')}}@endif">
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if(isset($product))
                                {{ method_field('PUT') }}
                            @endif
                            <button type="submit" id="submit" class="btn btn-success">Submit</button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@stop