@extends('layouts.default')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4>Product List</h4>
                <div class="table-responsive">
                    <table id="mytable" class="table table-bordred table-striped">
                        <thead>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </thead>
                        <tbody>
                        @foreach($products as $key => $row)

                            <tr>
                                <td>{{$row['name']}}</td>
                                <td>{{$row['description']}}</td>
                                <td>{{$row['price']}}</td>
                                <td>
                                    <a href="{{route('products.edit',['id' => encrypt($row['id'])])}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar </a>
                                </td>
                                <td>
                                    <form id="{{$key}}" action="{{ route('products.destroy', ['id' => encrypt($row['id'])]) }}" method="POST">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                        <a onclick=" document.getElementById({{$key}}).submit();" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Deletar</a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                        <a href="{{route('products.create')}}" class="btn btn-primary">Register</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop