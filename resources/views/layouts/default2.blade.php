<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>{{ config('app.name', 'Desafio Goviba') }} - @yield('title')</title>
    @include('includes.head')
</head>
<body>
<div class="container">
    <header class="row">
        @include('includes.header')
    </header>
    <div id="main" class="row">
        <!-- sidebar content -->
        <div id="sidebar" class="col-md-4">
            @include('includes.sidebar')
        </div>
        <!-- main content -->
        <div id="content" class="col-md-8">
            @yield('content')
        </div>
    </div>
    <footer class="row">
        @include('includes.footer')
    </footer>
</div>
<!-- App Script -->
{{--<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>--}}
</body>
</html>
